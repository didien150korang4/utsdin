<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DatapiController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/datapeserta', [DatapiController::class, 'datapeserta'])->name('datapeserta');

Route::get('/tambahdata', [DatapiController::class, 'tambahdata'])->name('tambahdata');
Route::post('/insertdata', [DatapiController::class, 'insertdata'])->name('insertdata');

Route::get('/tampildata/{id}', [DatapiController::class, 'tampildata'])->name('tampildata');
Route::post('/updatedata/{id}', [DatapiController::class, 'updatedata'])->name('updatedata');

Route::get('/delete/{id}', [DatapiController::class, 'delete'])->name('delete');
