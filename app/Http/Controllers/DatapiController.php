<?php

namespace App\Http\Controllers;

use App\Models\Datapi;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;
use laravel\sail\console\PublishCommand;

class DatapiController extends Controller
{
    public function datapeserta()
    {
        $data = Datapi::all();
        return view('datapeserta', compact('data'));
    }
    public function tambahdata()
    {
        return view('tambahdata');
    }
    public function insertdata(Request $request)
    {
        Datapi::create($request->all());
        return redirect()->route('datapeserta')->with('success', 'Data Berhasil Di Tambahkan');
    }
    public function tampildata($id)
    {
        $data = Datapi::find($id);
        return view('tampildata', compact('data'));
    }
    public function updatedata(Request $reques, $id)
    {
        $data = Datapi::find($id);
        $data->update($reques->all());
        return redirect()->route('datapeserta')->with('success', 'Data Berhasil Di Update');
    }
    public function delete($id)
    {
        $data = Datapi::find($id);
        $data->delete();
        return redirect()->route('datapeserta')->with('success', 'Data Berhasil Di Hapus');
    }
}
