<?php

namespace Database\Seeders;

use App\Models\Datapi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatapesertaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Datapi();
        $data->create([
            'nama' => 'Siti Rumjarum',
            'alamat' => 'Jonggol',
            'jeniskelamin' => 'cewe',
            'Notelepon' => '082330709378',
            'Dilegasi' => 'Universitas jonggol',
        ]);
    }
}
