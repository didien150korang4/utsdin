<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BinjaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('binjais')->insert([
            'nama' => 'Siti Rumjarum',
            'jeniskelamin' => 'cowo',
            'Notelepon' => '082330709378',
        ]);
    }
}
