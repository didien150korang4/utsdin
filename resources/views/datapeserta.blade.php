<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>CRUD LARAVEL 8</title>
</head>

<body>

    
    <h1 class="text-center mb-4">Data Peserta</h1>
    <div class="container">
        <a href="/tambahdata" button type="button" class="btn btn-success">Tambah +</a>
        <div class="row">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">no</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Jenis Kelamin</th>
                    <th scope="col">No Telepon</th>
                    <th scope="col">Dilegasi</th>
                    <th scope="col">Di Input</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        @foreach ($data as $row)
                        <tr>
                          <th scope="row">{{ $loop -> iteration }}</th>
                          <td>{{ $row->nama }}</td>
                          <td>{{ $row->alamat }}</td>
                          <td>{{ $row->jeniskelamin }}</td>
                          <td>0{{ $row->notelepon }}</td>
                          <td>{{ $row->Dilegasi }}</td>
                          <td>{{ $row->created_at->format('D M Y') }}</td>                         
                          <td>
                        
                        <a href="/tampildata/{{$row->id}}" class="btn btn-info">Edit</a>
                        <a href="/delete/{{$row->id}}"class="btn btn-danger">Dellete</a>
                    </td>
                  </tr>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </div>
    </div>

</body>

</html>